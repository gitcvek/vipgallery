<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php echo CHtml::encode($this->getPageTitle()); ?></title>
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <link rel="shortcut icon" href="../../flat-ui/images/favicon.ico">
        <link rel="stylesheet" href="../../flat-ui/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="../../flat-ui/css/flat-ui.css">
        <!-- Using only with Flat-UI (free)-->
        <link rel="stylesheet" href="../../common-files/css/icon-font.css">
        <!-- end -->
        <link rel="stylesheet" href="../../common-files/css/animations.css">
        
        <!-- If you don't use any of these blocks just remove unnecessary CSS files -->
        <!-- blog -->
        <link rel="stylesheet" href="../../ui-kit/ui-kit-blog/css/style.css">
        <!-- contacts -->
        <link rel="stylesheet" href="../../ui-kit/ui-kit-contacts/css/style.css">
        <!-- content -->
        <link rel="stylesheet" href="../../ui-kit/ui-kit-content/css/style.css">
        <!-- crew -->
        <link rel="stylesheet" href="../../ui-kit/ui-kit-crew/css/style.css">
        <!-- footer -->
        <link rel="stylesheet" href="../../ui-kit/ui-kit-footer/css/style.css">
        <!-- header -->
        <link rel="stylesheet" href="../../ui-kit/ui-kit-header/css/style.css">
        <!-- price -->
        <link rel="stylesheet" href="../../ui-kit/ui-kit-price/css/style.css">
        <!-- projects -->
        <link rel="stylesheet" href="../../ui-kit/ui-kit-projects/css/style.css">
        <?php
  //Регистрируем файлы скриптов в <head>
  if (YII_DEBUG) {
    Yii::app()->assetManager->publish(YII_PATH.'/web/js/source', false, -1, true);
  }
  Yii::app()->clientScript->registerCoreScript('jquery');
  ?>
    </head>

    <body>
        
        <div class="page-wrapper">



     <!-- header-10 -->
      <header class="header-10">
        <div class="container">
          <div class="row">
            <div class="navbar col-sm-12" role="navigation">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle"></button>
              </div>
              <div class="collapse navbar-collapse pull-right">
                <?php
                  $this->widget('MenuWidget', array(
                    'rootItem' => Yii::app()->menu->all,
                    'htmlOptions' => array('class' => 'nav pull-left'), // корневой ul
                    //'submenuHtmlOptions' => array('class' => 'dropdown-menu'), // все ul кроме корневого
                    'activeCssClass' => 'active', // активный li
                    //'activateParents' => 'true', // добавлять активность не только для конечного раздела, но и для всех родителей
                    //'labelTemplate' => '{label}', // шаблон для подписи
                    //'labelDropDownTemplate' => '{label} <b class="caret"></b>', // шаблон для подписи разделов, которых есть потомки
                    //'linkOptions' => array(), // атрибуты для ссылок
                    //'linkDropDownOptions' => array('data-target' => '#', 'class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'), // атрибуты для ссылок для разделов, у которых есть потомки
                    //'itemOptions' => array(), // атрибуты для li
                    //'itemDropDownOptions' => array('class' => 'dropdown'),  // атрибуты для li разделов, у которых есть потомки
                    'maxChildLevel' => 1,
                    //'encodeLabel' => false,
                  ));
                ?>
                <form class="navbar-form pull-left">
                  <a class="btn btn-success" href="#order">Подать заявку</a>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="header-background"></div>
      </header>
      <section class="header-10-sub v-center">
        <div class="background">&nbsp;</div>
        <div>
          <div class="container">
            <div class="hero-unit">
              <h1>Инвестиции в живопись выгодны!</h1>
              <p>Вложения в произведения искусства не обесценятся.</p>
            </div>
          </div>
        </div>
        <a class="control-btn fui-arrow-down" href="#info"></a>
      </section>
   
     
      
      
       <!-- content-14 -->
      <section class="content-14 v-center" id="info">
        <div>
          <div class="container">
            <h3>Выгодное вложение</h3>
            <p class="lead">7 причин начать коллекционировать произведения искусства</p>
            <div class="row features">
              <div class="col-sm-2">
                <div class="box-1">
                  <h6>Это выгодно</h6>
Вложения в живопись не обесцениваются с течением времени. Цена на антикварную живопись с 2009 по 2013 год увеличилась на 700%.
                </div>
                <div class="box-2">
                  <h6>Социальный статус</h6>
Иметь подлинное произведение искусства в своем доме, всегда считалось свидетельством социального успеха, символом респектабельности и тонкого вкуса.
                </div>
                <div class="box-3">
                  <h6>Изысканный интерьер</h6>
Потребность в гармонии, красоте и произведениях искусства свойственна любому человеку.
Приятно находиться рядом с подлинными свидетельствами движений человеческой души.
                </div>
                <div class="box-4">
                  <h6>Художественный вкус</h6>
Художественный вкус формируется только через общение с миром искусства, то есть во время общения с другими коллекционерами, художниками и при посещении выставок.
                </div>
              </div>
              <div class="col-sm-6 col-sm-offset-1">
                <img src="../../common-files/img/content/page.png" alt="">
              </div>
              <div class="col-sm-2 col-sm-offset-1">
                <div class="box-5">
                  <h6>Прекрасный подарок</h6>
Купить картину в подарок близкому человеку, что может быть лучше? Подлинник картины покажет, как важен вам человек и будет напоминать о вас на протяжении всей жизни. 
                </div>
                <div class="box-6">
                  <h6>Высокие перспективы</h6>
Наши специалисты готовы порекомендовать наилучшие способы реализации произведений искусства: галереи, аукционы, другие инвесторы.
Многие инвесторы, которые сегодня добились больших высот, начинали с нуля: проводили консультации со специалистами, 
определяя тот стиль, который, по их мнению, способен был принести наибольшую прибыль.
                </div>
                <div class="box-7">
                  <h6>Начать легко</h6>
Нужно всего лишь обратиться к нам за консультацией. Наши специалисты помогут сделать правильный выбор и вам останется только купить понравившуюся картину и наслажадаться результатом!
                </div>
      </section>


  <!-- projects-3 -->
      <section class="projects-3" id="about">
        <div class="container">
          <h3>Наша команда поможет вам стать<br> экспертом в мире искусства</h3>
          <div class="projects">
            <div class="project-wrapper">
              <div class="project">
                <div class="photo-wrapper">
                  <div class="photo">
                    <img src="../../common-files/img/projects/img-2.png" alt="">
                  </div>
                  <div class="overlay"><span class="fui-eye"></span>
                  </div>
                </div>
                <div class="info">
                  <div class="name">Экспертная группа</div>
                 Получите подробные экспертные заключения о подлинности живописных полотен.
                </div>
              </div>
            </div>
            <div class="project-wrapper">
              <div class="project">
                <div class="photo-wrapper">
                  <div class="photo">
                    <img src="../../common-files/img/projects/img-3.png" alt="">
                  </div>
                  <div class="overlay"><span class="fui-eye"></span>
                  </div>
                </div>
                <div class="info">
                  <div class="name">Консультанты</div>
                  Группа консультантов всегда готова разъяснить все тонкости процесса подбора произведений искусства, их покупки, хранения и дальнейшей реализации.
                </div>
              </div>
            </div>
            <div class="project-wrapper">
              <div class="project">
                <div class="photo-wrapper">
                  <div class="photo">
                    <img src="../../common-files/img/projects/img-4.png" alt="">
                  </div>
                  <div class="overlay"><span class="fui-eye"></span>
                  </div>
                </div>
                <div class="info">
                  <div class="name">Искусствоведы</div>
                  Мы обладаем аналитической информацией об осуществляемых сделках с произведениями искусства, чтобы помочь сделать правильный выбор.
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--/.container-->
      </section>
      
      
       <!-- contacts-1 -->
      
      <?php $this->widget('ygin.modules.feedback.widgets.FeedbackWidget'); ?>
      
      
       <!-- footer-11 -->
      <footer class="footer-11">
        <div class="container">
          <a class="brand" href="#">
          </a>
          <nav>
            <?php
                  $this->widget('MenuWidget', array(
                    'rootItem' => Yii::app()->menu->all,
                    //'htmlOptions' => array('class' => 'nav pull-left'), // корневой ul
                    //'submenuHtmlOptions' => array('class' => 'dropdown-menu'), // все ul кроме корневого
                    'activeCssClass' => 'active', // активный li
                    //'activateParents' => 'true', // добавлять активность не только для конечного раздела, но и для всех родителей
                    //'labelTemplate' => '{label}', // шаблон для подписи
                    //'labelDropDownTemplate' => '{label} <b class="caret"></b>', // шаблон для подписи разделов, которых есть потомки
                    //'linkOptions' => array(), // атрибуты для ссылок
                    //'linkDropDownOptions' => array('data-target' => '#', 'class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'), // атрибуты для ссылок для разделов, у которых есть потомки
                    //'itemOptions' => array(), // атрибуты для li
                    //'itemDropDownOptions' => array('class' => 'dropdown'),  // атрибуты для li разделов, у которых есть потомки
                    'maxChildLevel' => 1,
                    //'encodeLabel' => false,
                  ));
                ?>
          </nav>
        </div>
        <div class="additional-links">
          <div class="container">
            <p class="lead">Более <b>100</b> коллекционеров уже сотрудничают с нашими консультантами.</p>
          </div>
        </div>
      </footer>
      
       
        </div>

        <!-- Placed at the end of the document so the pages load faster -->
        <!--<script src="../../common-files/js/jquery-1.10.2.min.js"></script>-->
        <script src="../../flat-ui/js/bootstrap.min.js"></script>
        <script src="../../common-files/js/modernizr.custom.js"></script>
        <script src="../../common-files/js/page-transitions.js"></script>
        <script src="../../common-files/js/startup-kit.js"></script>
    </body>
</html>