//daAlert v3.0 http://cvek.ru
function daAlert(titleHTML, contentHTML, buttonText, messageClass) {
  var alertHTML = '<div class="modal fade" id="daAlert">'+
  '<div class="modal-dialog">'+
    '<div class="modal-content">'+
      '<div class="modal-header">'+
        '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
        '<h4 class="modal-title">'+titleHTML+'</h4>'+
      '</div>'+
      '<div class="modal-body">'+contentHTML+'</div>'+
      '<div class="modal-footer">'+
        '<button type="button" class="btn btn-default" data-dismiss="modal">'+buttonText+'</button>'+
      '</div>'+
    '</div><!-- /.modal-content -->'+
  '</div><!-- /.modal-dialog -->'+
'</div><!-- /.modal -->';
  $('#daAlert').remove();
  $('body').prepend(alertHTML);
  $("#daAlert").modal('show').alert();
}